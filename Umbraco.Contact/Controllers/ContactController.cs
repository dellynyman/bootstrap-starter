﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Contact.Models;
using Umbraco.Core.Models;
using Umbraco.Web.Mvc;

namespace Umbraco.Contact.Controllers
{
    public class ContactController : SurfaceController
    {
        public ActionResult RenderContactForm()
        {
            return PartialView("contactForm", new ContactFormModel());
        }

        [HttpPost]
        public ActionResult HandleContactForm(ContactFormModel contact)
        {
            if (!ModelState.IsValid)
            {
                return CurrentUmbracoPage();
            }

            IContent doc = ApplicationContext.Services.ContentService.CreateContent("Contact", contact.ContactOverviewId,
                "ContactItem");

            doc.Name = contact.Name + ": " + contact.Subject;
            doc.SetValue("name", contact.Name);
            doc.SetValue("email", contact.Email);
            doc.SetValue("subject", contact.Subject);
            doc.SetValue("message", contact.Message);
            doc.CreateDate = DateTime.Now;

            ApplicationContext.Services.ContentService.SaveAndPublishWithStatus(doc);

            TempData["IsSuccessful"] = true;

            return RedirectToCurrentUmbracoPage();
        }
    }
}
