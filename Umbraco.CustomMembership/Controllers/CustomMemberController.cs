﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Xml;
using umbraco.cms.businesslogic.media;
using umbraco.cms.presentation.create.controls;
using Umbraco.Core.Services;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using UmbracoExamine.DataServices;
using MediaType = umbraco.cms.businesslogic.media.MediaType;
using Member = umbraco.cms.businesslogic.member.Member;

namespace Umbraco.CustomMembership.Controllers
{
    public class CustomMemberController : SurfaceController
    {
        [HttpPost]
        public ActionResult HandleUpdateProfile([Bind(Prefix = "profileModel")] ProfileModel model)
        {
            if (ModelState.IsValid)
            {
                string mediaPath = "";
                Media m = null;

                if (Request.Files.Count > 0)
                {
                    var file = Request.Files[0];

                    if (file != null && file.ContentLength > 0)
                    {
                        string _text = file.FileName;
                        string filename;
                        string _fullFilePath = "";

                        filename =
                            _text.Substring(_text.LastIndexOf("\\") + 1, _text.Length - _text.LastIndexOf("\\") - 1)
                                .ToLower();

                        m = Media.MakeNew(
                        filename, MediaType.GetByAlias("image"), umbraco.BusinessLogic.User.GetUser(0), 1200);


                        string mediaRootPath = Server.MapPath("~/media") + "\\"; // get path from App_GlobalResources
                        string storagePath = mediaRootPath + m.Id.ToString();
                        System.IO.Directory.CreateDirectory(storagePath);
                        _fullFilePath = storagePath + "\\" + filename;
                        file.SaveAs(_fullFilePath);

                        string orgExt = ((string)_text.Substring(_text.LastIndexOf(".") + 1, _text.Length - _text.LastIndexOf(".") - 1));
                        orgExt = orgExt.ToLower();
                        string ext = orgExt.ToLower();
                        try
                        {
                            m.getProperty("umbracoExtension").Value = ext;
                        }
                        catch { }

                        // Save file size
                        try
                        {
                            System.IO.FileInfo fi = new FileInfo(_fullFilePath);
                            m.getProperty("umbracoBytes").Value = fi.Length.ToString();
                        }
                        catch { }

                        // Check if image and then get sizes, make thumb and update database
                        if (",jpeg,jpg,gif,bmp,png,tiff,tif,".IndexOf("," + ext + ",") > 0)
                        {
                            int fileWidth;
                            int fileHeight;

                            FileStream fs = new FileStream(_fullFilePath,
                                FileMode.Open, FileAccess.Read, FileShare.Read);

                            System.Drawing.Image image = System.Drawing.Image.FromStream(fs);
                            fileWidth = image.Width;
                            fileHeight = image.Height;
                            fs.Close();
                            try
                            {
                                m.getProperty("umbracoWidth").Value = fileWidth.ToString();
                                m.getProperty("umbracoHeight").Value = fileHeight.ToString();
                            }
                            catch { }

                            // Generate thumbnails
                            string fileNameThumb = _fullFilePath.Replace("." + orgExt, "_thumb");
                            generateThumbnail(image, 100, fileWidth, fileHeight, _fullFilePath, ext, fileNameThumb + ".jpg");

                            image.Dispose();
                        }
                        mediaPath = "/media/" + m.Id.ToString() + "/" + filename;

                        m.getProperty("umbracoFile").Value = mediaPath;
                        m.XmlGenerate(new XmlDocument());
                        m.Save();
                    }
                }

                var member = Member.GetCurrentMember();
                if (member != null)
                {
                    if (model.Name != null)
                    {
                        member.Text = model.Name;
                    }
                    member.Email = model.Email;
                    if (model.MemberProperties != null)
                    {
                        foreach (var property in model.MemberProperties.Where(p => p.Value != null))
                        {
                            member.getProperty(property.Alias).Value = property.Value;
                        }
                    }
                    if(mediaPath != "")
                        member.getProperty("avatar").Value = mediaPath;
                    
                    member.Save();
                    member.XmlGenerate(new XmlDocument());
                    Member.AddMemberToCache(member);

                    Response.Redirect(model.RedirectUrl);
                }
            }

            return RedirectToCurrentUmbracoPage();
        }

        protected void generateThumbnail(System.Drawing.Image image, int maxWidthHeight, int fileWidth, int fileHeight, string fullFilePath, string ext, string thumbnailFileName)
        {
            // Generate thumbnail
            float fx = (float)fileWidth / (float)maxWidthHeight;
            float fy = (float)fileHeight / (float)maxWidthHeight;
            // must fit in thumbnail size
            float f = Math.Max(fx, fy); //if (f < 1) f = 1;
            int widthTh = (int)Math.Round((float)fileWidth / f); int heightTh = (int)Math.Round((float)fileHeight / f);

            // fixes for empty width or height
            if (widthTh == 0)
                widthTh = 1;
            if (heightTh == 0)
                heightTh = 1;

            // Create new image with best quality settings
            Bitmap bp = new Bitmap(widthTh, heightTh);
            Graphics g = Graphics.FromImage(bp);
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;

            // Copy the old image to the new and resized
            Rectangle rect = new Rectangle(0, 0, widthTh, heightTh);
            g.DrawImage(image, rect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel);

            // Copy metadata
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            ImageCodecInfo codec = null;
            for (int i = 0; i < codecs.Length; i++)
            {
                if (codecs[i].MimeType.Equals("image/jpeg"))
                    codec = codecs[i];
            }

            // Set compresion ratio to 90%
            EncoderParameters ep = new EncoderParameters();
            ep.Param[0] = new EncoderParameter(Encoder.Quality, 90L);

            // Save the new image
            bp.Save(thumbnailFileName, codec, ep);
            bp.Dispose();
            g.Dispose();

        }

    }
}
